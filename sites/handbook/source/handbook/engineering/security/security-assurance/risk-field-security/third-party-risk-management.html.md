---
layout: markdown_page
title: "Third Party Risk Management"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Purpose

In order to minimize the risk associated with third party applications and services, the Risk and Field Security Team has designed this procedure to document the intake, assessment, and monitoring of Third Party Risk Management activities.

## Scope 

The Third Party Risk Management procedure is applicable to any third party application or service being provided to GitLab. This includes, but is not limited to, third parties providing free or paid applications or software, professional services organizations and contractors, marketing service providers and field marketing, alliances and partnerships, and mergers and acquisitions. 

## Roles and Responsibilities

| Role | Responsibility |
| ------ | ------ |
| **Risk and Field Security team** | Maintain a mechanism to intake and respond to Third Party Risk Management Activities |
| | Provide complete and accurate responses within documented SLA |
| | Document and report any risks or trends identified during Third Party Risk Management Activities |
| | Maintain Metrics |
| **Business or System Owner** | Submit request to Risk and Field Security|
| | Work with the R&FS team to complete the assessment |
| | Accept or Remediate any observations identified |


## Third Party Risk Management Activities Workflow

Regardless of the type of Third Party being assessed, it is important to document the inherent risk they might pose to GitLab. In order to centralize this process, the Risk and Field Security will intake and assess **all third party requests** based on a common methodology. It is important to note that the Third Party Risk Management procedure is meant to function **alongside** of GitLab's procurement and contracting processes and is not a **replacement** for any other required reviews and approvals. 

```mermaid
graph TD
subgraph Legend
	Link[These are clickable boxes]
	end
  POS[Purchase Request:<br>Software]-->1
  FRAP[Free Software] -->1
  POPS[Purchase Request:<br>Prof Serv/Contractors]-->1
  PMF[Purchase Request:<br>Field Mktg & Events]-->1
  ALLI[Alliances]-->1
  MRG[Mergers &<br>Acquisitions] -->1
  1[Submit Request] 
  1--> B{Risk Assessment}
  B -->|Low| D[No Further Action]
  B -->|Moderate| E[Security Assessment and BitSight Required]
  B -->|High| F[Security Assessment, BitSight and<br/>App Sec Review<br/>Required]

click POS "https://about.gitlab.com/handbook/finance/procurement/vendor-contract-saas/"
style POS fill:#EAF2FB, stroke:#2266AA, stroke-width:2px

click POPS "https://about.gitlab.com/handbook/finance/procurement/vendor-contract-professional-services/"
style POPS fill:#EAF2FB, stroke:#2266AA, stroke-width:2px

click PMF "https://about.gitlab.com/handbook/finance/procurement/vendor-contract-marketing/"
style PMF fill:#EAF2FB, stroke:#2266AA, stroke-width:2px

click FRAP "https://about.gitlab.com/handbook/communication/#need-to-add-a-new-app-to-slack"
style FRAP fill:#EAF2FB, stroke:#2266AA, stroke-width:2px

style Link fill:#EAF2FB, stroke:#2266AA, stroke-width:2px
```

## Risk Assessment

GitLab Team Members will follow the [Purchase Request for Software](https://about.gitlab.com/handbook/finance/procurement/vendor-contract-saas/), [Free Software/Apps](https://about.gitlab.com/handbook/communication/#need-to-add-a-new-app-to-slack), [Professional Services and Contractors](https://about.gitlab.com/handbook/finance/procurement/vendor-contract-professional-services/) and [Field Marketing and Events](https://about.gitlab.com/handbook/finance/procurement/vendor-contract-marketing/) process to submit a request for a new or renewing third party. 

Automation then creates a [Third Party Risk Management (TPRM)](https://gitlab.com/gitlab-com/gl-security/security-assurance/risk-field-security-team/third-party-vendor-security-management/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=) issue. The issue template documents key information on the Organization, Reputation, Business Continuity, Legal and Regulatory, Financial, and Customer and Stakeholder impact of the third party on GitLab. 

The Risk and Field Security team converts the answers provided in the TPRM issue into numeric values based on the chart below. The values are then added together to determine the third party's inherent risk. **Note**: Data Classification is weighted 10X higher than the other questions. This ensures that if red data is in-scope, it will guarantee a high inherent risk score and thus require a full security assessment.

<p>
<details markdown="1">
<Summary>Additional Details</summary>

### Third Party Risk Methodology

| Numeric Value | Organizational Impact | Reputational Impact |Business Continuity Impact |  Legal & Regulatory Impact | Financial Impact |Customers & Stakeholders Impact |
| --- | -- | --- | --- | ---- | ---- | --- |
| 1 | Number of licenses or seats is less than 20% of the total company size | Volume of data, records or external users such as event attendees is no more than 100 |	No system integrations | No compliance frameworks in scope | Contract is up to $999| No data involved | 
| 2 | Number of licenses or seats is 30% - 40% | Volume of data, records or external users is no more than 500   | 1 system integration|  1 compliance framework in scope | Contract is between $1,000 and $9,999 |Green data classification |
| 3 | Number of licenses or seats is 40% - 50%	| Volume of data, records or external users is no more than 1,000 | 2 system integrations | 2 compliance frameworks in scope | Contract is between $10,000 and $29,999 |Yellow data classification | 
| 4 | Number of licenses or seats is 50% - 75% | Volume of data, records or external users is no more than 10,000  | 3 system integrations | 3 compliance frameworks in scope | Contract is between $30,000 and $99,999 |Orange data classification |
| 5 | Number of licenses or seats is 75%+ | Volume of data, records or external users is more than 20,000 | 4+ system integrations	| 4+ compliance frameworks in scope | Contract is for $100,000+ | Red data classification |

---
### Inherent Risk Calculation

**Inherent Risk Formula**= Organizational Impact + Reputation Impact + Business Continuity Impact + Legal and Compliance Impact + Financial Impact + (Data Classification *10). 

---
### Risk Assessment Results and Actions

| Inherent Risk Score | Inherent Risk Tier | Action |
| ------ | ------ | ------ |
| Less than 30 | Low | No further action required |
| 31-44 | Moderate | Security Assessment and BitSight Required |
| Over 45 | High | Security Assessment, BitSight, and Technical Review Required |

</details></p>

## Security Assessment 

If a Security Assessment is deemed as necessary, the Risk and Field Security team conducts the following steps:

1. Reviews independent audits such as SOC1 Type 2, SOC2 Type 2 or similar, as well as any applicable bridge letters. If a third party does not have a SOC report (or similar), a questionnaire will be required based on the [Third Party Minimum Security Standards](/handbook/engineering/security/security-assurance/risk-field-security/third-party-minimum-security-standards.html). If the third party is in scope for SOX, any CUECs will be mapped to the applicable GitLab control within ZenGRC. 
1. Reviews external testing such as independent vulnerability scan reports or penetration test reports
1. Utilizes BitSight to obtain the security rating and documents any adverse findings. Third Parties with significant adverse findings will be escalated to the Security Operations and Research Team for a deeper technical assessment. 
1. Documents control effectiveness and ability to meet [Third Party Minimum Security Standards](/handbook/engineering/security/security-assurance/risk-field-security/third-party-minimum-security-standards.html)
1. Follows [GitLab's Observation Management](https://gitlab.com/gitlab-com/gl-security/security-assurance/sec-compliance/observation-management/-/blob/master/ZenGRC%20Issue%20(Observation)%20Field%20Definitions.md) process to report risks. 
1. Documents the Residual Risk (residual risk is calculated in the same manner as inherent risk, but the liklihood and impact is reassessed based on the known existing controls, processes/procedures, etc. that reduce/mitigate the risk). 

<p>
<details markdown="1">
<Summary>Additional Details</summary>

### New Third Parties and Systems

1. Only R&FS can create a new Third Party or System in ZenGRC
1. New Third Parties are created with the `Pending Assessment` status until the Residual Risk is determined, then it will change to `Accepted`. Third Parties that GitLab no longer utilizes will be changed to `Rejected`.
1. New Systems are created with the `Draft` status until the Residual Risk is determined, then it will be change to `Final`. Systems that GitLab no longer utilizes will have the `Not in Scope` status.
1. All Systems must be mapped to at least one Third Party; Not all Third Parties will be mapped to a System (example: professional services)
1. All Systems and Third Parties should be mapped to an Org Group

### Activity Location and Statuses
 
**GitLab**

1. Business Owner opens GitLab Procurement Issue
1. Automation creates a TPRM Issue for each of the GitLab issues noted above
1. Business Owner completes the **General Information** Section of the TPRM issue
1. R&FS Calculates the Inherent Risk
1. R&FS ensures all areas in the `General Information` and `Inherent Risk Calculation` sections are completed, applies the applicable Inherent Risk Label, and notifies the Business Owner in the original GitLab issue of any next steps. 
1. R&FS closes the TPRM Issue and opens a ZenGRC Audit (pending future automation). Audits for LOW Inherent Risk third parties are automatically closed within ZenGRC as no further action is required. MODERATE and HIGH Inherent Risk third party audits are opened in `Draft` status. 
 
**ZenGRC**

1. R&FS sends `Documentation Request` questionnaire in ZenGRC to the security contact for the third party. **NOTE**: The Audit will stay in `Draft` with no auditor assigned until the questionnaire is completed. 
1. R&FS adds the `Documents Requested` tag to the audit. R&FS ensures the Inherent Risk is set on the Audit object and the proper Time Period tag is applied in ZenGRC
1. Once the documents are received, R&FS moves the audit to `In Progress` and assigns the next available auditor. 
1. R&FS completes the audit and documents and Issues or Risk based on [GitLab's Observation Management](https://gitlab.com/gitlab-com/gl-security/security-assurance/sec-compliance/observation-management/-/blob/master/ZenGRC%20Issue%20(Observation)%20Field%20Definitions.md)
1. 1. R&FS documents the Residual Risk on the audit object
1. R&FS exports the Audit Report results to the [TPRM Template](https://drive.google.com/drive/u/1/folders/1my1LuYJZzz64SwWgrGw1wcFN6_WXVoYT) and attaches a PDF copy to the audit in ZenGRC
 
**GitLab**

1. R&FS completes the `Residual Risk Score and Outstanding Observations` section of the TPRM issue
1. R&FS attaches a PDF copy of the Audit Report to the TPRM Issue and ensures the applicable Residual Risk label is applied. 
1. R&FS utilizes the original Procurement issue to notify the Business Owner of any further action

### Quality Checklist

**TPRM Issue**
 
- [ ] All impact Score checkboxes are set
- [ ] Inherent and Residual Risk labels are applied
- [ ] Any Issues are added to the `Residual Risk Score and Outstanding Observations` section
- [ ] Final report is linked in the comments
 
**ZenGRC Audit**
 
- [ ] Audit Category, map:Section, map:Vendor, and map:System are set
- [ ] Time Period tag is applied and all other tags are removed
- [ ] Inherent and Residual Risk are set on the audit object
- [ ] Any Issues or Risks are mapped to the Audit
- [ ] A PDF copy of the Final Report is attached, and a link is included in the comments
- [ ] If CUEC mapping was done, the CUEC Import File is attached (not a link, the actual file)
 
**ZenGRC Vendor**
 
- [ ] Inherent and Residual Risk and map:System are set
- [ ] Any System Integrations (internal) or Sub-service Providers (such as AWS or GPC) are mapped
- [ ] The Status is set to `Accepted`
- [ ] The Vendor’s Security Contact information is included in the `description` field

**ZenGRC System**

- [ ] Data Classification, Critical System Tier, Compliance Requirements, and Subprocessor status are set
- [ ] map:Vendor is set
- [ ] Any System Integrations (internal) or Sub-service Providers (such as AWS or GPC) are mapped
- [ ] The Status is set to `Final`

</details></p>

## Email Request Template

>As part of GitLab's Third Party Risk Management program, our Risk and Field Security team assesses Third Parties and their applications or services, as applicable, to ensure they meet our [Third Party Minimum Security Standards](/handbook/engineering/security/security-assurance/risk-field-security/third-party-minimum-security-standards.html). Please complete the linked Document Request as soon as possible so we can begin our Security Assessment. If you do not have these resources, please email security-assurance@gitlab.com and we will send you a link to our Third Party Security Assessment questionnaire. We appreciate your partnership in helping GitLab complete its due diligence requirements.

## Frequently Asked Questions

**1. How long does the Risk Assessment Process Take?** The [Risk Assessment](https://about.gitlab.com/handbook/engineering/security/security-assurance/risk-field-security/third-party-risk-management.html#risk-assessment) actually takes less than 5 minutes! The Risk and Field Security team utilizes an automated formula that calculates the Inherent Risk based on information you provide to us in the **General Information** Section of the [Third Party Risk Management issue template](https://gitlab.com/gitlab-com/gl-security/security-assurance/risk-field-security-team/third-party-vendor-security-management/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=). The sooner that information is provided, the sooner we can calculate the Inherent Risk and provide you with next steps.
 
**2. What’s the difference between a Risk Assessment and a Security Assessment?** The purpose of the [Risk Assessment](https://about.gitlab.com/handbook/engineering/security/security-assurance/risk-field-security/third-party-risk-management.html#risk-assessment) is to do an initial vetting of all Third Parties to determine how their security practices might impact GitLab. Some Third Parties are more risky than others based on the type of Data they have access to, any systems they might integrate with, and their regulatory compliance requirements. The Risk and Field Security team uses the Methodology noted below to determine each Third Party’s inherent risk. Third Parties with Moderate or High inherent risk will require a Security Assessment. The purpose of the [Security Assessment](https://about.gitlab.com/handbook/engineering/security/security-assurance/risk-field-security/third-party-risk-management.html#security-assessment) is to dive deeper into the Third Party’s security controls and ensure they can meet our [Third Party Minimum Security standards](https://about.gitlab.com/handbook/engineering/security/security-assurance/risk-field-security/third-party-minimum-security-standards.html). 
 
**3. What is inherent risk? And why do we need to calculate it?** Inherent Risk is the risk before considering any existing mitigations in place, such as technical controls, internal processes or procedures, and independent auditing. We calculate this for each Third Party so that we can ensure that the high risk Third Parties are reviewed more thoroughly while lower risk Third Parties can be approved quickly. 
 
**4. How often do you review Third Parties?** As noted above, the Third Party Risk Management process is divided into two pieces- the [Risk Assessment](https://about.gitlab.com/handbook/engineering/security/security-assurance/risk-field-security/third-party-risk-management.html#risk-assessment) and the [Security Assessment](https://about.gitlab.com/handbook/engineering/security/security-assurance/risk-field-security/third-party-risk-management.html#security-assessment). The Risk Assessment is done with **every new procurement request**. Automation is built into the procurement templates to automatically generate a Third Party Risk Management issue for each request. This may seem inefficient given the amount of repeat requests for certain Third Parties, however, it is critical that this is done to validate the Data Classification and previously assigned inherent risk. Oftentimes scope changes between engagements with Third Parties and we need to ensure that this is a factor in our Risk Assessment. As noted above, this process takes less than 5 minutes to complete once all information is provided. A Security Assessment, however, is the more thorough review that is done for any Moderate or High inherent risk Third Parties. This review is required annually and the Risk and Field Security team has a 10 day SLA from the time documentation is received from the Third Party.
 
**5. What can I do to support this process?** To make the process run as smoothly as possible, the first step is to complete the **General Information** Section of the Third Party Risk Management issue as soon as possible. It is important to provide contact information for the Third Party so that, if it is determined that a Security Assessment is needed, we can initiate the document request quickly.

## Coming soon

* Instructions for reporting a security incident with a Third Party
* Exceptions to the Third Party Minimum Security Standards

## Service Level Agreements

Once the documentation is is received, the Risk and Field Security will complete the Security Assessment within **10 business days**. If the Third Party does not respond within that timeframe, any open items will be documented as Observations and presented to the requestor. 

## Exceptions

Not Applicable

## References
[Third Party Minimum Security Standards](/handbook/engineering/security/security-assurance/risk-field-security/third-party-minimum-security-standards.html)
